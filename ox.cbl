       IDENTIFICATION DIVISION. 
       PROGRAM-ID. OX.
       AUTHOR. KUNTAPONG.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  WS-ROW OCCURS 3 TIMES.
           05 WS-COL PIC X OCCURS 3 TIMES VALUE "-".

       01  WS-IDX-ROW PIC 9.
       01  WS-IDX-COL PIC 9. 
       01  WS-INPUT-ROW PIC 9.
           88 WS-INPUT-ROW-VALID VALUE 1 THRU 3.
       01  WS-INPUT-COL PIC 9.
           88 WS-INPUT-COL-VALID VALUE 1 THRU 3.
       01  WS-COUNT PIC 9(2) VALUE ZERO. 
       01  WS-PLAYER PIC X VALUE "X".

       01  COUNT-DRAW PIC 9.
       01  STATUS-WIN-LOSE PIC X VALUE "T".
       01  COUNT-OX PIC 9.
       01  LAST-ROW PIC 9 VALUE 1.
       01  LAST-COL PIC 9 VALUE 1.
       01  CURR-PLAYER PIC X.
       01  NUMBER-COUNT PIC 9.

       PROCEDURE DIVISION. 
       BEGIN. 
           PERFORM UNTIL WS-COUNT>9
              PERFORM DISPLAY-TURN 
              PERFORM INPUT-ROW-COL       
              PERFORM PUT-TABLE 
              PERFORM CHECK-WIN
              PERFORM DISPLAY-TABLE
              PERFORM RESET-ROW-COL 
           END-PERFORM
           GOBACK
           .
       PUT-TABLE.
           IF WS-COL(WS-INPUT-ROW, WS-INPUT-COL) = "-"
              MOVE WS-PLAYER TO WS-COL(WS-INPUT-ROW, WS-INPUT-COL)
              ADD 1 TO WS-COUNT
              ADD 1 TO COUNT-DRAW
              MOVE WS-COL(WS-INPUT-ROW, WS-INPUT-COL) TO CURR-PLAYER
              COMPUTE LAST-ROW = WS-INPUT-ROW
              COMPUTE LAST-COL = WS-INPUT-COL
              PERFORM TURN-PLAYER 
           ELSE
              IF STATUS-WIN-LOSE NOT = "T"
                 DISPLAY "ERROR" 
              END-IF 
           END-IF
           
           .
       CHECK-WIN.
           PERFORM CHECK-ROW 
           PERFORM CHECK-COL 
           PERFORM CHECK-X 
           PERFORM CHECK-DRAW
           .

       CHECK-ROW.
           MOVE "T" TO STATUS-WIN-LOSE 
           PERFORM VARYING WS-IDX-COL FROM 1 BY 1
              UNTIL WS-IDX-COL > 3 
                   IF WS-COL(LAST-ROW, WS-IDX-COL)
                      IS NOT EQUAL TO CURR-PLAYER 
                      MOVE "F" TO STATUS-WIN-LOSE  
                   END-IF 
           END-PERFORM
           IF STATUS-WIN-LOSE = "T"
              DISPLAY CURR-PLAYER " WIN!!!"
              PERFORM PUT-TABLE 
              PERFORM DISPLAY-TABLE 
              STOP RUN
           END-IF 
           .
       CHECK-COL.
           MOVE "T" TO STATUS-WIN-LOSE 
           PERFORM VARYING WS-IDX-ROW FROM 1 BY 1
              UNTIL WS-IDX-ROW > 3 
                   IF WS-COL(WS-IDX-ROW, LAST-COL)
                      IS NOT EQUAL TO CURR-PLAYER 
                      MOVE "F" TO STATUS-WIN-LOSE  
                   END-IF 
           END-PERFORM
           IF STATUS-WIN-LOSE = "T"
              DISPLAY CURR-PLAYER " WIN!!!"
              PERFORM PUT-TABLE 
              PERFORM DISPLAY-TABLE 
              STOP RUN
           END-IF 
           .
       CHECK-X.
           MOVE "T" TO STATUS-WIN-LOSE 
           PERFORM VARYING WS-IDX-ROW FROM 1 BY 1
              UNTIL WS-IDX-ROW > 3 
                   IF WS-COL(WS-IDX-ROW, WS-IDX-ROW)
                      IS NOT EQUAL TO CURR-PLAYER 
                      MOVE "F" TO STATUS-WIN-LOSE  
                   END-IF 
           END-PERFORM
           IF STATUS-WIN-LOSE = "T"
              DISPLAY CURR-PLAYER " WIN!!!"
              PERFORM PUT-TABLE 
              PERFORM DISPLAY-TABLE 
              STOP RUN
           END-IF 

           MOVE "T" TO STATUS-WIN-LOSE 
           PERFORM VARYING WS-IDX-ROW FROM 1 BY 1
              UNTIL WS-IDX-ROW > 3 
                   COMPUTE NUMBER-COUNT = 5 - 1 - WS-IDX-ROW 
                   IF WS-COL(WS-IDX-ROW, NUMBER-COUNT)
                      IS NOT EQUAL TO CURR-PLAYER 
                      MOVE "N" TO STATUS-WIN-LOSE  
                   END-IF 
           END-PERFORM
           IF STATUS-WIN-LOSE = "T"
              DISPLAY CURR-PLAYER " WIN!!!"
              PERFORM PUT-TABLE 
              PERFORM DISPLAY-TABLE 
              STOP RUN
           END-IF 
           .
       CHECK-DRAW.
           IF COUNT-DRAW >= 9 THEN
              DISPLAY "!!!DRAW!!!"
              STOP RUN
           END-IF
           .

       DISPLAY-TURN.
           DISPLAY "TURN " WS-PLAYER
           .
       TURN-PLAYER. 
           IF WS-PLAYER = "X"
              MOVE "O" TO WS-PLAYER 
           ELSE
              MOVE "X" TO WS-PLAYER 
           END-IF
           .
       RESET-ROW-COL.
           MOVE ZERO TO WS-INPUT-ROW, WS-INPUT-COL
           .
       INPUT-ROW-COL. 
           PERFORM UNTIL WS-INPUT-ROW-VALID
                   DISPLAY "INPUT ROW -"
                   ACCEPT WS-INPUT-ROW 
           END-PERFORM 
           PERFORM UNTIL WS-INPUT-COL-VALID
                   DISPLAY "INPUT COL -"
                   ACCEPT WS-INPUT-COL 
           END-PERFORM
           .
       DISPLAY-TABLE. 
           PERFORM VARYING WS-IDX-ROW FROM 1 BY 1
              UNTIL WS-IDX-ROW > 3 
                   PERFORM VARYING WS-IDX-COL FROM 1 BY 1
                      UNTIL WS-IDX-COL > 3 
                           DISPLAY WS-COL(WS-IDX-ROW, WS-IDX-COL)
                              WITH NO ADVANCING 
                   END-PERFORM
                   DISPLAY " " 
           END-PERFORM
           .
